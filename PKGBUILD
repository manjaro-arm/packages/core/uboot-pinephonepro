# U-Boot: Pinephone Pro based on PKGBUILD for RK3399
# Maintainer: Philip Müller <philm@manjaro.org>
# Contributor: Furkan Kardame <furkan@fkardame.com>
# Contributor: Dan Johansen <strit@manjaro.org>
# Contributor: Dragan Simic <dsimic@buserror.io>

pkgname=uboot-pinephonepro
pkgver=2022.04
pkgrel=3
epoch=5
_tfaver=2.7
pkgdesc="U-Boot for Pine64 PinePhone Pro"
arch=('aarch64')
url='https://source.denx.de/u-boot/u-boot'
license=('GPL')
makedepends=('git' 'arm-none-eabi-gcc' 'dtc' 'bc')
depends=('uboot-tools')
provides=('uboot')
conflicts=('uboot')
install=${pkgname}.install
source=("ftp://ftp.denx.de/pub/u-boot/u-boot-${pkgver/rc/-rc}.tar.bz2"
        "https://git.trustedfirmware.org/TF-A/trusted-firmware-a.git/snapshot/trusted-firmware-a-${_tfaver}.tar.gz"
        "boot.txt"
        "ppp-prepare-fstab"
        "ppp-prepare-fstab.service"
        "ppp-uboot-flash"
        "ppp-uboot-mkscr"
        "1001-Correct-boot-order-to-be-USB-SD-eMMC.patch"
        "1002-rockchip-Add-initial-support-for-the-PinePhone-Pro.patch"
        #"1003-Configure-USB-power-settings-for-PinePhone-Pro.patch"
        "1004-mtd-spi-nor-ids-Add-GigaDevice-GD25LQ128E-entry.patch"
        "1005-Reconfigure-GPIO4_D3-as-input-on-PinePhone-Pro.patch"
        "2001-mmc-sdhci-allow-disabling-sdma-in-spl.patch"
        "2002-ram-rk3399-Fix-.set_rate_index-error-handling.patch"
        "2003-ram-rk3399-Fix-faulty-frequency-change-reports.patch"
        "2004-ram-rk3399-Conduct-memory-training-at-400MHz.patch"
        "3001-pinephone-pro-Remove-cargo-culted-iodomain-config.patch"
        "3002-pine64-pinephonePro-SPI-support.patch")
sha256sums=('68e065413926778e276ec3abd28bb32fa82abaa4a6898d570c1f48fbdb08bcd0'
            '53422dc649153838e03820330ba17cb10afe3e330ecde0db11e4d5f1361a33e6'
            '12311da7e1a8d7bf19ddf78568e58da0888b0ca89e937aecccae4d896d20b7e2'
            'de7e36cdc7ed2fb5abb9155c97f87926361aa5be87d794c9016776160f3430ec'
            'e55fb02dfb6213eabbb899b468dc5f68d36a11c05feda4c14e80282415222fea'
            '6265fb9d3bc84bf1217383b52587b1d5a36372d88a824932586a802a502f62ba'
            '05eaccb2e8ea1eba3e86a4e7fcf12fd232195b5018c049ddf36e5a82a968cc24'
            '017d33aac55f8a5ed22170c97b4792ba755a4dad04f6c0cdd85119bbc81e87b3'
            '7c3d76f4bee0e54900142043241248072e334387065212141e1f600afe0aafba'
            '1c9cc403f527733b9ef093505c147074ac5afa881380d65ae3819f2bad75c8cd'
            '4417d39d9da79e3256267adb9928cd9fd7cf10d363761cea77b275f106804fe8'
            '7014c3f1ada93536787a4ce30b484dfe651c339391bd46869c61933825a0edcc'
            '8ac90e3f4b7c58d1092d6c21c7650dfa9019729bc94baa7a105df397c12c02a9'
            'b7efb8295351e3c358babc65b622cf52bd3adc2d1e97f11324f3f85e03af2101'
            'bffd549a4ac34a3ff43839d344609305dcea0b05e061854cb3b26325e6b23053'
            '29fb86661c0229fa115c490aea0425d498f0c53c05551d6d2abba048e17eb01a'
            '72bef2b7c69e63e219fc74ae887cf0c36a89fb203e0381285cd2b66d035c4b68')

prepare() {
  apply_patches() {
      local PATCH
      for PATCH in "${source[@]}"; do
          PATCH="${PATCH%%::*}"
          PATCH="${PATCH##*/}"
          [[ ${PATCH} = $1*.patch ]] || continue
          msg2 "Applying patch: ${PATCH}..."
          patch -N -p1 < "../${PATCH}" || true
      done
  }

  cd u-boot-${pkgver/rc/-rc}
  
  # Master branch patches
  apply_patches 0

  # Patches specific to the PinePhone Pro
  # NOTE: Patch "1003-Configure-USB-power-settings-for-PinePhone-Pro.patch"
  #       is disabled temporarily because it's currently not in usable state,
  #       but it will be reworked later
  apply_patches 1

  # Patches from the U-Boot repository or mailing list
  apply_patches 2

  # Temporary patches that will be folded into the first patch group later,
  # together with updating the device tree for the PinePhone Pro
  # TODO: While folding, also remove the now unused "pmugrf" variable in
  #       board/pine64/pinephone-pro-rk3399/pinephone-pro-rk3399.c
  apply_patches 3
}

build() {
  # Avoid build warnings by editing a .config option in place instead of
  # appending an option to .config, if an option is already present
  update_config() {
    if ! grep -q "^$1=$2$" .config; then
      if grep -q "^# $1 is not set$" .config; then
        sed -i -e "s/^# $1 is not set$/$1=$2/g" .config
      elif grep -q "^$1=" .config; then
        sed -i -e "s/^$1=.*/$1=$2/g" .config
      else
        echo "$1=$2" >> .config
      fi
    fi
  }

  unset CFLAGS CXXFLAGS CPPFLAGS LDFLAGS

  cd trusted-firmware-a-${_tfaver}

  echo -e "\nBuilding TF-A for Pine64 PinePhone Pro...\n"
  make PLAT=rk3399
  cp build/rk3399/release/bl31/bl31.elf ../u-boot-${pkgver/rc/-rc}

  cd ../u-boot-${pkgver/rc/-rc}

  echo -e "\nBuilding U-Boot for Pine64 PinePhone Pro...\n"
  make pinephone-pro-rk3399_defconfig

  update_config 'CONFIG_IDENT_STRING' '" Manjaro Linux ARM"'
  update_config 'CONFIG_BOOTDELAY' '0'

  # Some DDR4 RAMs need training at 400 MHz to successfully boot
  update_config 'CONFIG_RAM_RK3399_LPDDR4' 'y'

  # Try a couple of SPI tweaks;  if actually needed, these options
  # will be propely moved to a PinePhone Pro configuration patch later
  update_config 'CONFIG_SPL_DM_SEQ_ALIAS' 'y'
  update_config 'CONFIG_SF_DEFAULT_BUS' '1'

  # Disable DMA for eMMC, to make suspend/resume work;  this option
  # will be propely moved to a PinePhone Pro configuration patch later
  update_config 'CONFIG_SPL_MMC_SDHCI_SDMA' 'n'

  make EXTRAVERSION=-${pkgrel}
}

package() {
  cd u-boot-${pkgver/rc/-rc}

  install -D -m 0644 idbloader.img u-boot.itb -t "${pkgdir}/boot"
  install -D -m 0644 "${srcdir}/boot.txt" -t "${pkgdir}/boot"
  install -D -m 0755 "${srcdir}/ppp-uboot-mkscr" -t "${pkgdir}/usr/bin"

  install -D -m 0755 "${srcdir}/ppp-prepare-fstab" -t "${pkgdir}/usr/bin"
  install -D -m 0644 "${srcdir}/ppp-prepare-fstab.service" -t "${pkgdir}/usr/lib/systemd/system"
  install -D -m 0755 "${srcdir}/ppp-uboot-flash" -t "${pkgdir}/usr/bin"
}
